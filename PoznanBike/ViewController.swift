//
//  ViewController.swift
//  PoznanBike
//
//  Created by Grzegorz Kurnatowski on 26.07.2018.
//  Copyright © 2018 Last Manifold. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

struct stationData: Codable{
    let features: [features]
}

struct features: Codable {
    let geometry: geometry
    let properties: properties
    let id: String
}

struct geometry: Codable {
    let coordinates: [Double]
    
}

struct properties: Codable {
    let free_racks: String
    let bikes: String
    let label: String
    let bike_racks: String
}


class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var MapView: MKMapView!
    @IBAction func myLocation(_ sender: UIButton) {
        var curLocButton:CLLocation!
        curLocButton = manager.location
        MapView.setRegion(MKCoordinateRegionMake(CLLocationCoordinate2DMake(curLocButton.coordinate.latitude, curLocButton.coordinate.longitude), MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)), animated: true)
    }
    
    let manager = CLLocationManager()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        self.MapView.showsUserLocation = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        var curLoc:CLLocation!
        curLoc = manager.location
        
       MapView.setRegion(MKCoordinateRegionMake(CLLocationCoordinate2DMake(curLoc.coordinate.latitude, curLoc.coordinate.longitude), MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)), animated: true)
        
        let url = URL(string: "http://www.poznan.pl/mim/plan/map_service.html?mtype=pub_transport&co=stacje_rowerowe")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard let data = data else {return}
            do{
                let getData:stationData = try JSONDecoder().decode(stationData.self, from: data)
                for i in 0...getData.features.count-1{
                let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(getData.features[i].geometry.coordinates[1], getData.features[i].geometry.coordinates[0])
                let annotation = MKPointAnnotation()
                annotation.coordinate = location
                annotation.title = getData.features[i].properties.label + "(" + getData.features[i].properties.bikes + ")"
                let bikeLoc = CLLocation(latitude: getData.features[i].geometry.coordinates[1], longitude: getData.features[i].geometry.coordinates[0])
                let distance = curLoc.distance(from: bikeLoc)
                    annotation.subtitle = "Odległość: " + String(format:"%.1f", distance) + "m"
                self.MapView.addAnnotation(annotation)
                                     }
                
            }
                
            catch let error{
                
                print("Error: ", error)
            
            }
            
            }.resume()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

